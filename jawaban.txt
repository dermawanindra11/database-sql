1. Buat Database
CREATE DATABASE myshop;

2. membuat Table di Dalam Database
-table users
CREATE TABLE users( id int PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, email varchar(255) NOT null, pasword varchar(255) NOT null );

-table categories
CREATE TABLE categories( id int PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null );

-table items
CREATE TABLE items( id int PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, description varchar(255) NOT null, price int NOT null, stock int NOT null, category_id int NOT null, FOREIGN key(category_id) REFERENCES categories(id) );

3. Memasukan Data pada Table
-data users
INSERT INTO users(name, email, pasword)VALUES("Jhon Doe","john@doe.com","john123");
INSERT INTO users(name, email, pasword)VALUES("Jane Doe","jane@doe.com","jenita123");

-data categories
INSERT INTO categories(name) VALUES("gadget"),("cloth"),("men"),("women"),("branded");

-data items
INSERT INTO items(name, description, price, stock, category_id)VALUES("Sumsang b50","hape keren dari merek sumsang","4000000","100","1");
INSERT INTO items(name, description, price, stock, category_id)VALUES("Uniklooh","baju keren dari brand ternama","500000","50","2");
INSERT INTO items(name, description, price, stock, category_id)VALUES("IMHO Watch","jam tangan anak yang jujur banget","2000000","10","1");

4. Mengamnbil Data dari Database
a. mengambil data users
SELECT name, email FROM users;

b. mengambil data items
poin 1 : SELECT * from items where price > 1000000;
poin 2 : SELECT * FROM items where name LIKE 'unik%';

c. menampilkan data items join dengan kategori
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name AS kategori from items INNER JOIN categories ON items.category_id = categories.id;

5. mengubah data dari database
UPDATE items set stock=70 where id = 1;